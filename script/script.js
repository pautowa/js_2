"use sctrict";

const numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?','0');

let personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false
};

if (personalMovieDB.count < 10){
    alert('Просмотрено довольно мало фильмов');
}
else if (personalMovieDB.count >= 10 && personalMovieDB.count <= 30){
    alert('Вы классический зритель');
}
else if (personalMovieDB.count > 30){
    alert('Вы киноман');
}
else{
    alert('Произошла ошибка');
}

let i = 0;
let key = '';
let value = '';

// вариант 1
while (i < 2){
    key = prompt('Один из последних просмотренных фильмов?','');
    value = prompt('На сколько оцените его?','0');
    if (key && value && (key.length<=50)){
        personalMovieDB.movies[key] = value;
        i += 1;
    }
};

// Вариант 2
// while (Object.keys(personalMovieDB.movies).length < 2){
//     key = prompt('Один из последних просмотренных фильмов?','');
//     value = prompt('На сколько оцените его?','0');
//     if (key && value && (key.length<=50)){
//         personalMovieDB.movies[key] = value;
//     }
// };

// Вариант 3
// while (true){
//     key = prompt('Один из последних просмотренных фильмов?','');
//     value = prompt('На сколько оцените его?','0');
//     if (key && value && (key.length<=50)){
//         personalMovieDB.movies[key] = value;
//         i += 1;
//     }
//     if(i==2){
//         break;
//     }
// };
